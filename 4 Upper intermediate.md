## Upper intermediate

### Check path in graph

Given an undirected graph, write a function which checks if it is possible to get from node A to node B.
Think about how would you represent such graph. Only use the standard library, no external modules.

*Optional: try doing it in a different graph representation as well*