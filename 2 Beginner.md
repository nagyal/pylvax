## Beginner

### Check permutations

Write a function which takes two strings, and returns whether the two are permutations of each other.

### [Text pyramid](https://gist.github.com/hyperknot/967d52d0b2337a60e7ca)

### Guess the number

1. The user should guess a number between 0 to 100.
2. The computer should be able to guess that number from maximum 7 questions.
3. The questions are yes/no questions.
4. Once the computer found out the number, print out that it won and what is the number.