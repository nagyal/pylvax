## Intermediate

### Fibonacci

Write a recursive function which returns the n-th Fibonacci number.

### Bin to dec and dec to bin

Write two functions, one which converts decimal numbers to binary and one which converts from binary numbers to decimal. Represent decimal numbers as integers and binary numbers as strings.

*Note: do not use the built-in number conversion functionality*